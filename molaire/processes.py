from multicorn import TableDefinition, ColumnDefinition
from multicorn.utils import log_to_postgres as log
import os
import os.path


def find_process_by_pid(pid, needs_hydration):
    exists = os.path.exists("/proc/{}".format(pid))
    if not exists:
        return

    process = {"pid": pid}
    if needs_hydration:
        hydrate_process(process)

    return process


def hydrate_process(process):
    replacements = {
        # On the left side is what we find in /proc/:pid/status, and
        # on the right side what we find in our table.
        "fdsize": "fd_size",
        "nstgid": "ns_tgid",
        "nspid": "ns_pid",
        "nspgid": "ns_pgid",
        "nssid": "ns_sid",
        "vmpeak": "vm_peak",
        "vmsize": "vm_size",
        "vmlck": "vm_lock",
        "vmpin": "vm_pin",
        "vmhwm": "vm_hwm",
        "vmrss": "vm_rss",
        "rssanon": "rss_anon",
        "rssfile": "rss_file",
        "rssshmem": "rss_shmem",
        "vmdata": "vm_data",
        "vmstk": "vm_stk",
        "vmexe": "vm_exe",
        "vmlib": "vm_lib",
        "vmpte": "vm_pte",
        "vmswap": "vm_swap",
        "hugetlbpages": "huge_tlb_pages",
        "coredumping": "core_dumping",
        "sigq": "sig_queued",
        "sigpnd": "sig_png",
        "shdpnd": "shd_pnd",
        "sigblk": "sig_blk",
        "sigign": "sig_ign",
        "sigcgt": "sig_cgt",
        "capinh": "cap_inh",
        "capprm": "cap_prm",
        "capeff": "cap_eff",
        "capbnd": "cap_bnd",
        "capamb": "cam_amb",
        "nonewprivs": "no_new_privs",
        "voluntary_ctxt_switches": "voluntary_context_switches",
        "nonvoluntary_ctxt_switches": "nonvoluntary_context_switches",
    }
    replacements_keys = replacements.keys()
    try:
        with open("/proc/{}/status".format(process["pid"])) as f:
            for line in f:
                field, value = line.split(":", 1)

                # Some basic transformations valid for all fields.
                field = field.lower()
                value = value.strip()

                # Map the real field names with our more friendly ones.
                if field in replacements_keys:
                    field = replacements[field]

                # Now the more special ones...
                # Let's start with the integer[]
                if field in {"uid", "gid", "groups", "ns_tgid", "ns_pid", "ns_pgid", "ns_sid"}:
                    value = map(lambda x: int(x), filter(lambda x: x, value.split()))

                # Then the "xx kB" ones
                if (
                        field.startswith("vm_")
                        or field.startswith("rss_")
                        or field in {"huge_tlb_pages"}
                ):
                    value = int(value.split()[0])
                    value *= 1024

                # The x/max_x one.
                if field == "sig_queued":
                    parts = value.split("/")
                    value = int(parts[0])
                    process["max_sig_queued"] = int(parts[1])

                # The text array
                if field == "speculation_store_bypass":
                    value = value.split()

                # The integers
                if field in {"tgid", "ngid", "pid", "ppid", "tracer_pid", "fd_size",
                             "threads", "seccomp", "voluntary_context_switches",
                             "nonvoluntary_context_switches"}:
                    value = int(value)

                # The octals
                if field in {"umask"}:
                    value = int(value, 8)

                # The hexs
                if field in {"sig_pnd", "shd_pnd", "sig_blk", "sig_ign", "sig_cgt",
                             "cap_inh", "cap_prm", "cap_eff", "cap_bnd", "cap_amb"}:
                    value = int(value, 16)

                # The booleans
                if field in {"core_dumping", "thp_enabled", "no_new_privs"}:
                    value = bool(value)

                process[field] = value or None
    except IOError:
        # Our reading is racy, we sometimes end up with outdated
        # processes. It's okay.
        return

    return process


def find_all_processes(needs_hydration):
    for pid in os.listdir("/proc"):
        if not pid.isdigit():
            continue
        process = {"pid": pid}
        if needs_hydration:
            hydrate_process(process)
        yield process


def process_matches_quals(process, quals):
    for qual in quals:
        if qual.operator == "=" and process[qual.field_name] != qual.value:
            return False
        if qual.operator == "!=" and process[qual.field_name] == qual.value:
            return False

    return True


class Processes(object):

    def execute(self, quals, columns):
        pids = []
        any = False
        for qual in quals:
            if qual.field_name != "pid":
                continue
            if isinstance(qual.operator, tuple):
                pids = qual.value
                any = qual.operator[1] == "ANY"
            elif qual.operator == "=":
                pids = [qual.value]

        if pids is not None and not any and len(pids) > 1:
            # This doesn't make sense.
            return

        needs_hydration = "pid" not in columns or len(columns) > 1

        if pids:
            # Fast path
            for pid in pids:
                process = find_process_by_pid(pid, needs_hydration)
                if process:
                    yield process

        else:
            for process in find_all_processes(needs_hydration):
                if process_matches_quals(process, quals):
                    yield process

    @classmethod
    def import_schema(cls, schema, srv_options, options, restriction_type, restricts):
        return TableDefinition(
            table_name="processes",
            columns=[
                ColumnDefinition(
                    column_name=definition[0],
                    type_name=definition[1],
                )
                for definition in [
                        ("name", "text"),
                        ("umask", "numeric"),
                        ("state", "text"),
                        ("tgid", "integer"),
                        ("ngid", "integer"),
                        ("pid", "integer"),
                        ("ppid", "integer"),
                        ("tracer_pid", "integer"),
                        ("uid", "integer[]"),
                        ("gid", "integer[]"),
                        ("fd_size", "integer"),
                        ("groups", "integer[]"),
                        ("ns_tgid", "integer[]"),
                        ("ns_pid", "integer[]"),
                        ("ns_pgid", "integer[]"),
                        ("ns_sid", "integer[]"),
                        ("vm_peak", "numeric"),
                        ("vm_size", "numeric"),
                        ("vm_lock", "numeric"),
                        ("vm_pin", "numeric"),
                        ("vm_hwm", "numeric"),
                        ("vm_rss", "numeric"),
                        ("rss_anon", "numeric"),
                        ("rss_file", "numeric"),
                        ("rss_shmem", "numeric"),
                        ("vm_data", "numeric"),
                        ("vm_stk", "numeric"),
                        ("vm_exe", "numeric"),
                        ("vm_lib", "numeric"),
                        ("vm_pte", "numeric"),
                        ("vm_swap", "numeric"),
                        ("huge_tlb_pages", "numeric"),
                        ("core_dumping", "boolean"),
                        ("thp_enabled", "boolean"),
                        ("threads", "integer"),
                        ("sig_queued", "integer"),
                        ("max_sig_queued", "integer"),
                        ("sig_pnd", "numeric"),
                        ("shd_pnd", "numeric"),
                        ("sig_blk", "numeric"),
                        ("sig_ign", "numeric"),
                        ("sig_cgt", "numeric"),
                        ("cap_inh", "numeric"),
                        ("cap_prm", "numeric"),
                        ("cap_eff", "numeric"),
                        ("cap_bnd", "numeric"),
                        ("cap_amb", "numeric"),
                        ("no_new_privs", "boolean"),
                        # boolean?
                        ("seccomp", "integer"),
                        ("speculation_store_bypass", "text[]"),
                        ("cpus_allowed_list", "text"),
                        ("mems_allowed_list", "text"),
                        ("voluntary_context_switches", "numeric"),
                        ("nonvoluntary_context_switches", "numeric"),
                ]
            ],
            options={
                "type": "processes",
            },
        )
