from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres as log

from .processes import Processes

TYPES = {
    "processes": Processes,
}


class Molaire(ForeignDataWrapper):

    def __init__(self, options, columns):
        super(Molaire, self).__init__(options, columns)

        self._obj = TYPES[options["type"]]()

    def execute(self, quals, columns):
        return self._obj.execute(quals, columns)

    @classmethod
    def import_schema(cls, schema, srv_options, options, restriction_type, restricts):
        return [
            cls.import_schema(schema, srv_options, options, restriction_type, restricts)
            for cls in TYPES.values()
        ]
